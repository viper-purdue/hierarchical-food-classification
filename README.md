# hierarchical-food-classification



## Introduction

This is the project for multi-stage hierarchical food classification. This is the link to our paper: https://arxiv.org/abs/2309.01075

The code for this paper is in the file: merge_item.py

The trained models are in the folder 'models'. 

## Cite our paper

@article{pan2023,
 author="Pan, Xinyue and He, Jiangpeng and Zhu, Fengqing",
 title="Muti-Stage Hierarchical Food Classification",
 journal="International Workshop on Multimedia Assisted Dietary Management (MADiMa '23)",
 year="2023"}



