import torch
from torch import nn
import torchvision.models as models
from torchvision import datasets, transforms, models
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from sklearn.cluster import AffinityPropagation
import numpy as np
from copy import deepcopy
from PIL import ImageFile
from PIL import Image
import os
import shutil
import time
from sklearn import manifold
from sklearn import decomposition
from sklearn.metrics import confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay
import matplotlib.pyplot as plt
from torchcam.methods import SmoothGradCAMpp
from torchvision.io.image import read_image
from torchvision.transforms.functional import normalize, resize, to_pil_image
import matplotlib.pyplot as plt
from torchcam.utils import overlay_mask
from scipy import stats
from scipy.integrate import quad
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.metrics import silhouette_score
from sklearn.metrics import davies_bouldin_score
from sklearn.metrics import mean_absolute_error

os.environ["CUDA_VISIBLE_DEVICES"] = "5"

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Look up relative nutritional composition data in FNDDS sheet
def lookup(nutrient,food_code):
    selected_feature = [4,5,6,9]
    for i in range(len(nutrient[0])):
        if food_code == nutrient[0][i]:
            nutrient_features = []
            for k in selected_feature:
                nutrient_features.append(nutrient[k][i])
            return nutrient_features
    return None

nutrient = pd.read_excel('FNDDS_Nutrient_Values_17_18.xlsx', index_col=None, header=None)  
food_types = os.listdir('./vfn_complete_hierarchical/images')
food_item_dict = dict()
food_nutrient = []
k = 0
for tp in food_types:
    for item in os.listdir('./vfn_complete_hierarchical/images/'+tp):
        if item not in food_item_dict:
            food_item_dict[item] = k
            temp_nutrient = lookup(nutrient,int(item))
            if temp_nutrient == None:
                print(tp,item,temp_nutrient)
            food_nutrient.append(temp_nutrient)
            k += 1
            
food_nutrient = np.array(food_nutrient)

# Get the learning rate of the model
def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']
    
# Normalize the image
def normalize_image(image):
    image_min = image.min()
    image_max = image.max()
    image.clamp_(min = image_min, max = image_max)
    image.add_(-image_min).div_(image_max - image_min + 1e-5)
    return image 

# Plot incorrect classification results
def plot_most_incorrect(incorrect, classes, n_images, name, normalize = True):
    
    rows = int(np.sqrt(n_images))
    cols = int(np.sqrt(n_images))

    fig = plt.figure(figsize = (25, 20))

    for i in range(rows*cols):

        ax = fig.add_subplot(rows, cols, i+1)
        
        image, true_label, probs = incorrect[i]
        image = image.permute(1, 2, 0)
        true_prob = probs[true_label]
        incorrect_prob, incorrect_label = torch.max(probs, dim = 0)
        true_class = classes[true_label]
        incorrect_class = classes[incorrect_label]

        if normalize:
            image = normalize_image(image)

        ax.imshow(image.cpu().numpy())
        ax.set_title(f'true label: {true_class} ({true_prob:.3f})\n' \
                     f'pred label: {incorrect_class} ({incorrect_prob:.3f})',fontsize=7)
        ax.axis('off')
        
    fig.subplots_adjust(hspace=0.4)
    fig.savefig(name)
    
def get_representations(model, iterator):
    
    model.eval()

    outputs = []
    intermediates = []
    labels = []

    with torch.no_grad():
        
        for (x, y, y_code) in iterator:

            x = x.to(device)

            y_pred = model(x)

            outputs.append(y_pred.cpu())
            labels.append(y_code)
        
    outputs = torch.cat(outputs, dim = 0)
    labels = torch.cat(labels, dim = 0)

    return outputs, labels

# TSNE representation
def get_tsne(data, n_components = 2, n_images = None):
    
    if n_images is not None:
        data = data[:n_images]
        
    tsne = manifold.TSNE(n_components = n_components, random_state = 0)
    tsne_data = tsne.fit_transform(data)
    print(tsne.kl_divergence_)
    return tsne_data

# PCA representation
def get_pca(data, n_components = 2):
    pca = decomposition.PCA()
    pca.n_components = n_components
    pca_data = pca.fit_transform(data)
    return pca_data

def plot_representations(data, labels, classes, n_images = None, name='tsne.png'):
            
    if n_images is not None:
        data = data[:n_images]
        labels = labels[:n_images]
                
    fig = plt.figure(figsize = (15, 15))
    ax = fig.add_subplot(111)
    scatter = ax.scatter(data[:, 0], data[:, 1], c = labels, cmap = 'hsv')
    #fig.savefig('base_tsne.png')
    handles, _ = scatter.legend_elements(num = None)
    legend = plt.legend(handles = handles, labels = classes)
    fig.savefig(name)

# Confusion matrix plot  
def plot_confusion_matrix(labels, pred_labels, classes, name):
    
    fig = plt.figure(figsize = (100, 100));
    ax = fig.add_subplot(1, 1, 1);
    cm = confusion_matrix(labels, pred_labels);
    cm = ConfusionMatrixDisplay(cm, display_labels = classes);
    cm.plot(values_format = 'd', cmap = 'Blues', ax = ax)
    fig.delaxes(fig.axes[1]) #delete colorbar
    plt.xticks(rotation = 90)
    plt.xlabel('Predicted Label', fontsize = 50)
    plt.ylabel('True Label', fontsize = 50)
    plt.xticks(size = 50)
    plt.yticks(size = 50)
    plt.savefig(name)

# Compute overlap coefficient  
def overlap_coefficient(feature_class1, feature_class2):
    # Fit Gaussian KDEs to the features for each class
    kde_class1 = stats.gaussian_kde(feature_class1)
    kde_class2 = stats.gaussian_kde(feature_class2)

    # Define a function that gives the minimum density at each point
    min_density = lambda x: min(kde_class1(x), kde_class2(x))

    # Integrate this function over all feature values to get the overlap coefficient
    overlap, _ = quad(min_density, -np.inf, np.inf)
    return overlap


# Clustering across different food types
def clustering_global(food_dataset,model,food_types, dim=2048):
    test_transforms = transforms.Compose([transforms.Resize(255),
                                          transforms.CenterCrop(224),
                                          transforms.ToTensor(),
                                          transforms.Normalize([0.485, 0.456, 0.406],
                                                               [0.229, 0.224, 0.225])])
    features = []
    labels_code = []
    labels_type = []
    food_types = os.listdir(food_dataset+'/train_set')
    k = 0
    for ft in food_types:
        temp_food_type_features = []
        temp_items = os.listdir(food_dataset+'/train_set/'+ft)
        for item in os.listdir(food_dataset+'/train_set/'+ft):
            temp_food_type_features_item = []
            for image in os.listdir(food_dataset+'/train_set/'+ft+'/'+item):
                file_name = food_dataset+'/train_set/'+ft+'/'+item+'/'+image
                temp_image = Image.open(file_name).convert('RGB')
                temp_image = test_transforms(temp_image)
                temp_image = np.array(temp_image,dtype='float')
                if dim == 2048:
                    output = model.resnet(torch.from_numpy(temp_image).cuda().float().unsqueeze(0))
                    output = output.detach().cpu().numpy().reshape((2048))
                if dim == 4096:
                    output_resnet = model.resnet(torch.from_numpy(temp_image).cuda().float().unsqueeze(0))
                    output_resnet = output_resnet.detach().cpu().numpy().reshape((2048))
                    output_trained_resnet50 = model.trained_resnet50(torch.from_numpy(temp_image).cuda().float().unsqueeze(0))
                    output_trained_resnet50 = output_trained_resnet50.detach().cpu().numpy().reshape((2048))
                    output = np.concatenate((output_resnet,output_trained_resnet50))
                temp_food_type_features_item.append(output)
            mean_feature = np.array(np.mean(np.array(temp_food_type_features_item),axis=0))
            var_feature = np.array(np.var(np.array(temp_food_type_features_item),axis=0))
            features.append(np.concatenate((mean_feature,var_feature)))
            labels_type.append(ft)
            labels_code.append(item)
    af = AffinityPropagation(damping=0.5)
    clusters = af.fit_predict(features)
    cluster_labels = np.unique(clusters)
    print('clustering silhouette score:',silhouette_score(features,clusters))
    print('clustering DBI',davies_bouldin_score(features,clusters))
    labels_code = np.array(labels_code)
    labels_type = np.array(labels_type)
    new_labels_type = []
    new_labels_item = []
    for lb in cluster_labels:
        selected_index = np.where(clusters == lb)[0]
        selected_items = labels_code[selected_index]
        selected_types = labels_type[selected_index]
        types_unique = np.unique(selected_types)
        for tp in types_unique:
            selected_index_item = np.where(selected_types == tp)[0]
            items_unique = np.unique(selected_items[selected_index_item])
            if len(items_unique) > 1:
                new_label = ""
                for it in items_unique:
                    new_label += it + '_'
                new_label = new_label[:-1]
            else:
                new_label = items_unique[0]
            new_labels_type.append(tp)
            new_labels_item.append(new_label)

    return new_labels_type, new_labels_item, labels_code

# Create the dataset 
def create_dataset(old_dataset_folder,new_dataset_folder,labels_type,labels_code_collection,labels_code_collection_2):
    # Step 5: Create a new dataset with the new labels
    if os.path.exists(new_dataset_folder) == False:
        os.makedirs(new_dataset_folder)
    label = 0
    replaced_label = []
    for ft, it in zip(labels_type,labels_code_collection):
        it_split = it.split('_')
        #print(ft,it)
        if len(it_split) > 1:
            if os.path.exists(new_dataset_folder+'/train_set/'+ft+'/'+it+'/') == False:
                if len(it) > 100:
                    it = str(label)
                    replaced_label.append(it)
                    os.makedirs(new_dataset_folder+'/train_set/'+ft+'/'+it+'/')
                    label += 1
            if os.path.exists(new_dataset_folder+'/val_set/'+ft+'/'+it+'/') == False:
                if len(it) > 100:
                    it = str(label)
                    replaced_label.append(it)
                    os.makedirs(new_dataset_folder+'/val_set/'+ft+'/'+it+'/')
                    label += 1
            for ite in it_split:
                folder_src = old_dataset_folder + '/train_set/'+ft+'/'+ite
                folder_dst = new_dataset_folder + '/train_set/'+ft+'/'+it+'/'+ite+'/'
                if os.path.exists(folder_dst) == False and os.path.exists(folder_src) == True:
                    shutil.copytree(folder_src,folder_dst)
                folder_src = old_dataset_folder + '/val_set/'+ft+'/'+ite
                folder_dst = new_dataset_folder + '/val_set/'+ft+'/'+it+'/'+ite+'/'
                if os.path.exists(folder_dst) == False and os.path.exists(folder_src) == True:
                    shutil.copytree(folder_src,folder_dst)
        else:
            if os.path.exists(old_dataset_folder+'/train_set/'+ft+'/') == False:
                if len(it) > 100:
                    it = str(label)
                    replaced_label.append(it)
                    os.makedirs(old_dataset_folder+'/train_set/'+ft+'/')
                    label += 1
            if os.path.exists(old_dataset_folder+'/val_set/'+ft+'/') == False:
                if len(it) > 100:
                    it = str(label)
                    replaced_label.append(it)
                    os.makedirs(old_dataset_folder+'/val_set/'+ft+'/')
                    label += 1
            folder_src = old_dataset_folder + '/train_set/'+ft+'/'+it
            folder_dst = new_dataset_folder + '/train_set/'+ft+'/'+it+'/'
            if os.path.exists(folder_dst) == False and os.path.exists(folder_src) == True:
                shutil.copytree(folder_src,folder_dst)
            folder_src = old_dataset_folder + '/val_set/'+ft+'/'+it
            folder_dst = new_dataset_folder + '/val_set/'+ft+'/'+it+'/'
            if os.path.exists(folder_dst) == False and os.path.exists(folder_src) == True:
                shutil.copytree(folder_src,folder_dst)  
    
    food_types = os.listdir(new_dataset_folder+'/val_set')
    for tp in food_types:
        val_food_items = os.listdir(old_dataset_folder + '/val_set')   
        for val_it in val_food_items:
            if val_it not in labels_code_collection_2:
                folder_src = old_dataset_folder + '/val_set/'+tp+'/'+val_it
                folder_dst = new_dataset_folder + '/val_set/'+tp+'/'+val_it+'/'
                if os.path.exists(folder_dst) == False and os.path.exists(folder_src) == True:
                    shutil.copytree(folder_src,folder_dst)  
    return replaced_label

# Load the dataset
class MyDataset(Dataset):
    def __init__(self, images, train=True):
        self.train = train
        self.train_transforms = transforms.Compose([transforms.RandomRotation(30),
                                           transforms.RandomResizedCrop(224),
                                           transforms.RandomHorizontalFlip(p=0.5), 
                                           transforms.AutoAugment(),
                                           transforms.ToTensor(),
                                           transforms.Normalize([0.485, 0.456, 0.406],
                                                                [0.229, 0.224, 0.225])])
        self.test_transforms = transforms.Compose([transforms.Resize(255),
                                          transforms.CenterCrop(224),
                                          transforms.ToTensor(),
                                          transforms.Normalize([0.485, 0.456, 0.406],
                                                               [0.229, 0.224, 0.225])])
        self.X_image = []
        self.Y = []
        self.Y_code = []
        for image in images:
            food_code = image[0].split('/')[-1].split('_')[1]
            self.X_image.append(image[0])
            self.Y.append(image[1])
            self.Y_code.append(food_item_dict[food_code])
        self.X_image = np.array(self.X_image)
        self.Y = torch.tensor(self.Y)
        self.Y_code = torch.tensor(self.Y_code)

    def __len__(self):
        return len(self.X_image)

    def __getitem__(self,index):
        image_path = self.X_image[index]
        label = self.Y[index]
        food_code = self.Y_code[index]

        image = Image.open(image_path).convert('RGB')
        if self.train:
            image = self.train_transforms(image)
        else:
            image = self.test_transforms(image)

        return image, label, food_code

class MyNewDataset(Dataset):
    def __init__(self,images,new_food_item_dict, train = True):
        self.train = train
        self.train_transforms = transforms.Compose([transforms.RandomRotation(30),
                                           transforms.RandomResizedCrop(224),
                                           transforms.RandomHorizontalFlip(p=0.5),  # ImageNetPolicy(),
                                           #transforms.RandomApply([transforms.ColorJitter(0.4,0.4,0.4,0.1)], p=0.5),
                                           #transforms.RandomGrayscale(p=0.5),
                                           transforms.AutoAugment(),
                                           transforms.ToTensor(),
                                           transforms.Normalize([0.485, 0.456, 0.406],
                                                                [0.229, 0.224, 0.225])])
        self.test_transforms = transforms.Compose([transforms.Resize(255),
                                          transforms.CenterCrop(224),
                                          transforms.ToTensor(),
                                          transforms.Normalize([0.485, 0.456, 0.406],
                                                               [0.229, 0.224, 0.225])])
        self.X_image = []
        self.Y = []
        self.Y_code = []
        for image in images:
            food_code = image[0].split('/')[4]
            #print('before',image[0])
            #print('after',image[0].split('/'))
            self.X_image.append(image[0])
            self.Y.append(image[1])
            self.Y_code.append(new_food_item_dict[food_code])
        self.Y = torch.tensor(self.Y)
        self.Y_code = torch.tensor(self.Y_code)

    def __len__(self):
        return len(self.X_image)

    def __getitem__(self,index):
        image_path = self.X_image[index]
        label = self.Y[index]
        food_code = self.Y_code[index]

        image = Image.open(image_path).convert('RGB')
        if self.train:
            image = self.train_transforms(image)
        else:
            image = self.test_transforms(image)
            
        return image, label, food_code

# Create the model
class Model(nn.Module):
    def __init__(self,resnet50,num_classes):
        super(Model, self).__init__()
        self.num_classes = num_classes
        self.resnet = resnet50
        self.final = nn.Sequential(nn.Linear(2048,512),nn.ReLU(),nn.Linear(512,num_classes))
        
    def forward(self,X_image):
        X_features = self.resnet(X_image)
        X_features = X_features.view(X_features.size(0),-1)
        Y_image = self.final(X_features)
        return Y_image
   
class multi_Model(nn.Module):
    def __init__(self,trained_resnet50,resnet50,num_classes):
        super(multi_Model, self).__init__()
        self.trained_resnet50 = trained_resnet50
        self.num_classes = num_classes
        self.resnet = resnet50
        self.final = nn.Sequential(nn.Linear(4096,512),nn.ReLU(),nn.Linear(512,num_classes))

    def forward(self,X_image):
        X_features_transfer = self.trained_resnet50(X_image)
        X_features_transfer = X_features_transfer.view(X_features_transfer.size(0),-1)
        X_features = self.resnet(X_image)
        X_features = X_features.view(X_features.size(0),-1)
        X_features_concat = torch.concat((X_features,X_features_transfer),dim=1)
        Y_image = self.final(X_features_concat)
        return Y_image

# Step 1: Load dataset       
train_transforms = transforms.Compose([transforms.RandomRotation(30),
                                           transforms.RandomResizedCrop(224),
                                           transforms.RandomHorizontalFlip(p=0.5),  # ImageNetPolicy(),
                                           transforms.AutoAugment(),
                                           transforms.ToTensor(),
                                           transforms.Normalize([0.485, 0.456, 0.406],
                                                                [0.229, 0.224, 0.225])])

test_transforms = transforms.Compose([transforms.Resize(255),
                                          transforms.CenterCrop(224),
                                          transforms.ToTensor(),
                                          transforms.Normalize([0.485, 0.456, 0.406],
                                                               [0.229, 0.224, 0.225])])

train_data = datasets.ImageFolder('./vfn_complete_hierarchical' + '/train_set', transform=train_transforms)
train_dataset = MyDataset(train_data.imgs)
train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True, num_workers=4)

# Also prepare a validation set
val_data = datasets.ImageFolder('./vfn_complete_hierarchical' + '/val_set', transform=test_transforms)
val_dataset = MyDataset(val_data.imgs,train=False)
val_loader = DataLoader(val_dataset, batch_size=32, shuffle=False, num_workers=4)

test_data = datasets.ImageFolder('./vfn_complete_hierarchical' + '/test_set', transform=test_transforms)
test_dataset = MyDataset(test_data.imgs,train=False)
test_loader = DataLoader(test_dataset, batch_size=16, shuffle=False)

# Step 2: Train initial model using food types as labels
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model_res = models.resnet50(pretrained=True)
for param in model_res.parameters():
    param.requires_grad = True
OUTPUT_DIM = len(food_item_dict)
print('Number of classes:', OUTPUT_DIM)
IN_FEATURES = model_res.fc.in_features
final_fc = nn.Linear(IN_FEATURES, OUTPUT_DIM)
model_res.fc = final_fc
modules = list(model_res.children())[:-1]
model_base = nn.Sequential(*modules)
model = Model(model_base,OUTPUT_DIM)
model = model.float()
model = model.to(device)
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.0001, betas=[0.9, 0.999])
num_epochs = 15
min_val_loss = float('inf')
running_loss = 0


old_dataset_folder = './vfn_complete_hierarchical'
new_dataset_folder = './v_hi_transfer_co_5'
init_lr = 0.0001
overall_min_val_loss_pre = float('inf')
overall_min_val_loss_type = float('inf')
overall_min_val_loss = float('inf')
overall_max_acc = 0
dictionary = dict()

for ft in os.listdir(old_dataset_folder+'/train_set'):
    dictionary[ft] = len(os.listdir(old_dataset_folder+'/train_set/'+ft))
print(dictionary)
temp_max_acc = []
temp_min_loss = []

# Start training
for j in range(5):
    print('Iteration number:',j+1)
    
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = model.to(device)
    if os.path.exists(new_dataset_folder) == True:
        shutil.rmtree(new_dataset_folder)
    # Step 3: Extract features for the images and do clustering on each food type
    model = model.eval()  # set model to evaluation model
    if j > 0:
        model = deepcopy(new_model_val)
        model.load_state_dict(torch.load('./models/res50_vfn_complete_flat_code_50_transfer_var_item_5.pth'))
    
    features = []
    labels = []
    labels_type, labels_code_collection, labels_code_collection_2 = clustering_global(
            old_dataset_folder,model,food_types, dim=2048)
    
    replaced_label = create_dataset(old_dataset_folder, new_dataset_folder,labels_type,labels_code_collection,labels_code_collection_2)
    
    dictionary = dict()
    for ft in os.listdir(new_dataset_folder+'/train_set'):
        dictionary[ft] = len(os.listdir(new_dataset_folder+'/train_set/'+ft))
    print(dictionary)        
            
    new_train_data = datasets.ImageFolder(new_dataset_folder+'/train_set', transform=train_transforms)
    new_food_item_dict = dict()
    k = 0
    for tp in food_types:
        for item in os.listdir(new_dataset_folder + '/train_set/'+tp):
            if item not in new_food_item_dict:
                new_food_item_dict[item] = k
                k += 1

    new_train_dataset = MyNewDataset(new_train_data.imgs,new_food_item_dict)
    new_train_loader = DataLoader(new_train_dataset, batch_size=32, shuffle=True,num_workers=4)

    new_val_data = datasets.ImageFolder(new_dataset_folder + '/val_set', transform=test_transforms)
    new_val_dataset = MyNewDataset(new_val_data.imgs,new_food_item_dict,train=False)
    new_val_loader = DataLoader(new_val_dataset, batch_size=32, shuffle=False,num_workers=4)
    
    # Step4: Train to classify food types
    OUTPUT_DIM = len(test_data.classes)
    model = Model(model.resnet,OUTPUT_DIM)
    model = model.float()
    model = model.to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=init_lr, betas=[0.9, 0.999])
    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, num_epochs, eta_min=0, last_epoch=-1, verbose=False)
    for epoch in range(num_epochs):
        model.train()
        start = time.time()
        for i, (inputs, labels, labels_code) in enumerate(train_loader):
            optimizer.zero_grad()
            # Move input and label tensors to the default device
            inputs, labels, labels_code = inputs.cuda(), labels.cuda(), labels_code.cuda()
            outputs = model(inputs.float())
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
            
        val_loss = 0
        accuracy = 0
        model.eval()
        with torch.no_grad():
            for i, (inputs, labels, labels_code) in enumerate(val_loader):
                inputs, labels, labels_code = inputs.cuda(), labels.cuda(), labels_code.cuda()
                outputs = model(inputs.float())
                loss = criterion(outputs, labels)
                val_loss += loss.item()
                
                # Calculate accuracy

                top_p, top_class = outputs.topk(1, dim=1)
                equals = top_class == labels.view(*top_class.shape)
                accuracy += torch.mean(equals.type(torch.FloatTensor)).item()

        if val_loss < overall_min_val_loss_type:
            print("Validation loss decreased  Saving model")
            overall_min_val_loss_type = val_loss
            best_model = deepcopy(model)
            torch.save(model.state_dict(), './models/res50_vfn_complete_flat_5.pth')
        
        print(f"Device = cuda; Time: {(time.time() - start):.3f} seconds")
        print(f"Epoch: {epoch + 1}/{num_epochs}.."
            f"Train loss: {running_loss / len(train_loader):.3f}.. "
            f"Validation loss: {val_loss / len(val_loader):.3f}.. "
            f"Validation accuracy: {accuracy / len(val_loader):.3f}")
        running_loss = 0
        scheduler.step()
        print('learning_rate', get_lr(optimizer))

    # Step5: Test on model trained for classifying food types
    model = best_model  
    model.load_state_dict(torch.load('./models/res50_vfn_complete_flat_5.pth'))
    valid_loss = 0
    accuracy = 0
    epoch_acc_5 = 0
    images_all = []
    labels_all = []
    probs = []
    with torch.no_grad():
        model.eval()
        for images, labels, labels_code in test_loader:
            images, labels, labels_code = images.cuda(), labels.cuda(), labels_code.cuda()
            logps = model(images)
            y_prob = F.softmax(logps, dim = -1)
            batch_loss = criterion(logps, labels)
            valid_loss += batch_loss.item()
            top_p, top_class = logps.topk(1, dim=1)
            _, top_pred = logps.topk(5, 1)
            top_pred = top_pred.t()
            correct = top_pred.eq(labels.view(1, -1).expand_as(top_pred))
            correct_5 = correct[:5].reshape(-1).float().sum(0, keepdim = True)
            acc_5 = correct_5 / labels.shape[0]
            epoch_acc_5 += acc_5.item()
            equals = top_class == labels.view(*top_class.shape)
            accuracy += torch.mean(equals.type(torch.FloatTensor)).item()
            images_all.append(images.cpu())
            labels_all.append(labels.cpu())
            probs.append(y_prob.cpu())
         
    epoch_acc_5 /= len(test_loader)
    print('Final test loss', valid_loss / len(test_loader))
    print('Final test accuracy:', accuracy / len(test_loader))
    print('Final test top 5 accuracy:', epoch_acc_5)
 
    # Step 6: Train a new model using these new labels(pseduo label created using clustering)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    OUTPUT_DIM = len(new_food_item_dict)
    new_model_base = nn.Sequential(*modules)
    for param in new_model.resnet.parameters():
        param.requires_grad = True
    new_model = new_model.float()
    new_model = new_model.to(device)
    optimizer = torch.optim.Adam(new_model.parameters(), lr=init_lr, betas=[0.9, 0.999])
    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, num_epochs, eta_min=0, last_epoch=-1, verbose=False)

    running_loss = 0
    min_val_loss = float('inf')
    for epoch in range(num_epochs):
        new_model.train()
        start = time.time()
        for i, (inputs, labels, labels_code) in enumerate(new_train_loader):
            optimizer.zero_grad()
            # Move input and label tensors to the default device
            inputs, labels, labels_code = inputs.cuda(), labels.cuda(), labels_code.cuda()
            outputs = new_model(inputs.float())
            loss = criterion(outputs, labels_code)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
            
        val_loss = 0
        accuracy = 0
        new_model.eval()
        with torch.no_grad():
            for i, (inputs, labels, labels_code) in enumerate(new_val_loader):
                inputs, labels, labels_code = inputs.cuda(), labels.cuda(), labels_code.cuda()
                outputs = new_model(inputs.float())
                loss = criterion(outputs, labels_code)
                val_loss += loss.item()
                
                # Calculate accuracy

                top_p, top_class = outputs.topk(1, dim=1)
                equals = top_class == labels_code.view(*top_class.shape)
                accuracy += torch.mean(equals.type(torch.FloatTensor)).item()

        if val_loss < overall_min_val_loss_pre:
            print("Validation loss decreased  Saving model")
            overall_min_val_loss_pre = val_loss
            new_best_model = deepcopy(new_model)
        
        print(f"Device = cuda; Time: {(time.time() - start):.3f} seconds")
        print(f"Epoch: {epoch + 1}/{num_epochs}.."
            f"Train loss: {running_loss / len(new_train_loader):.3f}.. "
            f"Validation loss: {val_loss / len(new_val_loader):.3f}.. "
            f"Validation accuracy: {accuracy / len(new_val_loader):.3f}")
        running_loss = 0
        scheduler.step()
        print('learning_rate', get_lr(optimizer))
           
    new_model = new_best_model
    
    # Step 7: Train and Validate on dataset with food items as label
    new_model_res_val = models.resnet50(pretrained=True)
    for param in new_model_res_val.parameters():
        param.requires_grad = True
    OUTPUT_DIM = len(food_item_dict)
    IN_FEATURES = new_model_res_val.fc.in_features
    final_fc = nn.Linear(IN_FEATURES, OUTPUT_DIM)
    new_model_res_val.fc = final_fc
    modules = list(new_model_res_val.children())[:-1]
    new_model_base_val = nn.Sequential(*modules)
    new_model_val = Model(new_model.resnet,OUTPUT_DIM)
    new_model_val = new_model_val.float()
    new_model_val = new_model_val.to(device)
    optimizer = torch.optim.Adam(new_model_val.parameters(), lr=init_lr, betas=[0.9, 0.999])
    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, num_epochs, eta_min=0, last_epoch=-1, verbose=False)
    min_val_loss = float('inf')
    
    for epoch in range(num_epochs):
        new_model_val.train()
        start = time.time()
        for i, (inputs, labels, labels_code) in enumerate(train_loader):
            optimizer.zero_grad()
            # Move input and label tensors to the default device
            inputs, labels, labels_code = inputs.cuda(), labels.cuda(), labels_code.cuda()
            outputs = new_model_val(inputs.float())
            loss = criterion(outputs, labels_code)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
            
        val_loss = 0
        accuracy = 0
        new_model_val.eval()
        predicted_energy_value = []
        actual_energy_value = []
        predicted_carbonhydrate_value = []
        actual_carbonhydrate_value = []
        predicted_fat_value = []
        actual_fat_value = []
        predicted_protein_value = []
        actual_protein_value = []
        with torch.no_grad():
            for i, (inputs, labels, labels_code) in enumerate(val_loader):
                inputs, labels, labels_code = inputs.cuda(), labels.cuda(), labels_code.cuda()
                outputs = new_model_val(inputs.float())
                loss = criterion(outputs, labels_code)
                val_loss += loss.item()
                
                # Calculate accuracy

                top_p, top_class = outputs.topk(1, dim=1)
                for k, l in zip(top_class.tolist(),labels_code.tolist()):
                    predicted_energy_value.append([food_nutrient[k[0]][0]])
                    actual_energy_value.append([food_nutrient[l][0]])
                    predicted_protein_value.append([food_nutrient[k[0]][1]])
                    actual_protein_value.append([food_nutrient[l][1]])
                    predicted_carbonhydrate_value.append([food_nutrient[k[0]][2]])
                    actual_carbonhydrate_value.append([food_nutrient[l][2]])
                    predicted_fat_value.append([food_nutrient[k[0]][3]])
                    actual_fat_value.append([food_nutrient[l][3]])
                equals = top_class == labels_code.view(*top_class.shape)
                accuracy += torch.mean(equals.type(torch.FloatTensor)).item()

        if overall_min_val_loss > val_loss: 
            print("Validation loss decreased  Saving model")
            min_val_loss = val_loss
            overall_min_val_loss = val_loss
            old_model_val = deepcopy(new_model_val)
            torch.save(new_model_val.state_dict(), './models/res50_vfn_complete_flat_code_50_transfer_var_item_5.pth')
        
        print(f"Device = cuda; Time: {(time.time() - start):.3f} seconds")
        print(f"Epoch: {epoch + 1}/{num_epochs}.."
            f"Train loss: {running_loss / len(new_train_loader):.3f}.. "
            f"Validation loss: {val_loss / len(new_val_loader):.3f}.. "
            f"Validation accuracy: {accuracy / len(new_val_loader):.3f}")
        print('Energy mean square error:',mean_absolute_error(actual_energy_value, predicted_energy_value))
        print('Protein mean square error:',mean_absolute_error(actual_protein_value, predicted_protein_value))
        print('carbonhydrate mean square error:',mean_absolute_error(actual_carbonhydrate_value, predicted_carbonhydrate_value))
        print('Fat mean square error:',mean_absolute_error(actual_fat_value, predicted_fat_value))
        running_loss = 0
        scheduler.step()
        print('learning_rate', get_lr(optimizer))
    
    # Validation on estimating nutrition composition
    new_model_val.load_state_dict(torch.load('./models/res50_vfn_complete_flat_code_50_transfer_var_item_5.pth'))
    valid_loss = 0
    accuracy = 0
    epoch_acc_5 = 0
    images_all = []
    labels_all = []
    probs = []
    predicted_energy_value = []
    actual_energy_value = []
    predicted_carbonhydrate_value = []
    actual_carbonhydrate_value = []
    predicted_fat_value = []
    actual_fat_value = []
    predicted_protein_value = []
    actual_protein_value = []
    with torch.no_grad():
        new_model_val.eval()
        for images, labels, labels_code in test_loader:
            images, labels, labels_code = images.cuda(), labels.cuda(), labels_code.cuda()
            logps = new_model_val(images)
            y_prob = F.softmax(logps, dim = -1)
            batch_loss = criterion(logps, labels_code)
            valid_loss += batch_loss.item()
            top_p, top_class = logps.topk(1, dim=1)
            for k, l in zip(top_class.tolist(),labels_code.tolist()):
                predicted_energy_value.append([food_nutrient[k[0]][0]])
                actual_energy_value.append([food_nutrient[l][0]])
                predicted_protein_value.append([food_nutrient[k[0]][1]])
                actual_protein_value.append([food_nutrient[l][1]])
                predicted_carbonhydrate_value.append([food_nutrient[k[0]][2]])
                actual_carbonhydrate_value.append([food_nutrient[l][2]])
                predicted_fat_value.append([food_nutrient[k[0]][3]])
                actual_fat_value.append([food_nutrient[l][3]])
            _, top_pred = logps.topk(5, 1)
            top_pred = top_pred.t()
            correct = top_pred.eq(labels_code.view(1, -1).expand_as(top_pred))
            correct_5 = correct[:5].reshape(-1).float().sum(0, keepdim = True)
            acc_5 = correct_5 / labels_code.shape[0]
            epoch_acc_5 += acc_5.item()
            equals = top_class == labels_code.view(*top_class.shape)
            accuracy += torch.mean(equals.type(torch.FloatTensor)).item()
            images_all.append(images.cpu())
            labels_all.append(labels_code.cpu())
            probs.append(y_prob.cpu())
        
    epoch_acc_5 /= len(test_loader)
    print('Damping:0.5')
    print('Final test loss', valid_loss / len(test_loader))
    print('Final test accuracy:', accuracy / len(test_loader))
    print('Final test top 5 accuracy:', epoch_acc_5)
    print('Energy mean square error:',mean_absolute_error(actual_energy_value, predicted_energy_value))
    print('Protein mean square error:',mean_absolute_error(actual_protein_value, predicted_protein_value))
    print('carbonhydrate mean square error:',mean_absolute_error(actual_carbonhydrate_value, predicted_carbonhydrate_value))
    print('Fat mean square error:',mean_absolute_error(actual_fat_value, predicted_fat_value))
    temp_min_loss.append(overall_min_val_loss)     
    if len(temp_min_loss) >= 3:
        print('Collection of validation loss:',temp_min_loss)
        if temp_min_loss[-1] >= temp_min_loss[-2] and temp_min_loss[-2] >= temp_min_loss[-3]:
            print('No improvement of validation accuracy any more')
            break
    
    init_lr = init_lr * 0.8